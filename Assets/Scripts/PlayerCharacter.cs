using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : BaseCharacter
{
    private float DefaultSpeed;
    private SwipeGestures swipe;
    float gravityForceAmount;
    public GameMode gameMode;
    private bool canDash = true;
    public PlayerUI playerUI;
    // Start is called before the first frame update
    void Start()
    {
        if(GameMode.GetDifficulty() == GameMode.Difficulty.Hard)
        {
            HP = 1;
        }
        else
        {
            HP = 2;
        }

        Debug.Log(GameMode.GetDifficulty());

        DefaultSpeed = getSpeed();

        setRB(GetComponent<Rigidbody2D>());
        customGravity = GetComponent<ConstantForce2D>();

        swipe = this.gameObject.GetComponent<SwipeGestures>();

        gravityForceAmount = GetRB().mass * Physics2D.gravity.magnitude;
        customGravity.force = new Vector2(gravityForceAmount, 0);
    }

    private void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CharacterMovement();
    }

    public override void CharacterMovement()
    {
        base.CharacterMovement();

        GetRB().velocity = new Vector2(GetRB().velocity.x, getSpeed());
        GetRB().AddForce(Vector2.right * 1.0f);

        if (swipe.getDirection() == Types.DirectionType.Tap)
        {
            StartCoroutine(PlayerDash(0.3f));
        }
    }

    public override void OnAttack (GameObject attackedCharacter, BaseCharacter attackedClass)
    {
        base.OnAttack(attackedCharacter, attackedClass);

        //transform.position = new Vector2(attackedCharacter.transform.position.x, attackedCharacter.transform.position.y);

        attackedClass.damageHP(1);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        playerUI.PlayerDeathUI();
        Time.timeScale = 0;
        Debug.Log("PlayerDies");

    }

    public override void damageHP(int Damage)
    {
        base.damageHP(Damage);

        if(getHp() > 0)
        {
            Stunned();
        }
    }

    IEnumerator PlayerDash(float delayTime)
    {
        setSpeed(5.0f);
        yield return new WaitForSeconds(delayTime);
        setSpeed(DefaultSpeed);
    }

    public void setCanDash(bool value) { canDash = value; }

    public void Stunned()
    {
        Debug.Log("damaged");
        Vector3 temp = transform.position;
        temp.y = temp.y - 5.0f;

        transform.position = temp;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) this.damageHP(1);
    }
}
