using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Text HPText;
    public Text ScoreText;
    public Text DifficultyText;

    public GameObject LoseScreen;
    public GameObject PlayerHUD;

    private PlayerCharacter player;
    // Start is called before the first frame update
    void Start()
    {
        player = this.gameObject.GetComponent<PlayerCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        HPText.text = "HP: " + player.getHp();
        ScoreText.text = "Score: " + GameMode.getScore();
    }
    public void PlayerDeathUI()
    {
        PlayerHUD.SetActive(false);
        LoseScreen.SetActive(true);
    }
}
