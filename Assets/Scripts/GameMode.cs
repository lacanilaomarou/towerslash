using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour
{
    public enum Difficulty
    {
        Easy=1,
        Medium=2,
        Hard=3,
    }
    // Start is called before the first frame update
    [SerializeField] static private Difficulty gameDifficulty = Difficulty.Easy;
    [SerializeField] static private float initialChanceHP = 0.1f;
    [SerializeField] static private float initialChanceSlowMo = 0.05f;
    [SerializeField] static private float initialChanceBoss = 0.05f;


    private static float realChanceHP = 0.0f;
    private static float realChanceSlowMO = 0.0f;
    private static float realChanceBoss = 0.0f;
    private static int totalScore;
    void Start()
    {
        ScaleChanceToDifficulty(gameDifficulty);
        totalScore = 0;
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void ScaleChanceToDifficulty(Difficulty dif)
    {
        switch(dif)
        {
            case Difficulty.Easy:
                realChanceHP = initialChanceHP;
                realChanceSlowMO = initialChanceHP + initialChanceSlowMo;
                break;

            case Difficulty.Medium:
                realChanceHP = 0.0f;
                realChanceSlowMO = initialChanceSlowMo;
                realChanceBoss = 0;
                break;

            case Difficulty.Hard:
                realChanceHP = 0.0f;
                realChanceSlowMO = 0.0f;
                realChanceBoss = initialChanceBoss;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("DDD");
        }
    }


    public static void setDifficulty(Difficulty d) { gameDifficulty = d; }
    public static Difficulty GetDifficulty() { return gameDifficulty; }
    public static float getChanceHP() { return realChanceHP; }
    public static float getChanceSlowMo() { return realChanceSlowMO; }
    public static float getChanceBoss() { return realChanceBoss; }

    public static void addScore(int value)
    {
        totalScore += value;
    }

    public static int getScore() { return totalScore; }
}
