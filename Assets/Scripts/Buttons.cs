using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    public Text DifficultyText;
    public Text FinalScoreText;
    // Start is called before the first frame update
    void Start()
    {
        DifficultyText.text = GameMode.GetDifficulty().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        FinalScoreText.text = GameMode.getScore().ToString();
    }

    public void onRestartBtnClick()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void EasyDifficultyBtn()
    {
        GameMode.setDifficulty(GameMode.Difficulty.Easy);
        GameMode.ScaleChanceToDifficulty(GameMode.GetDifficulty());
        DifficultyText.text = GameMode.GetDifficulty().ToString();
    }

    public void MediumDifficultyBtn()
    {
        GameMode.setDifficulty(GameMode.Difficulty.Medium);
        GameMode.ScaleChanceToDifficulty(GameMode.GetDifficulty());
        DifficultyText.text = GameMode.GetDifficulty().ToString();
    }

    public void HardDifficultyBtn()
    {
        GameMode.setDifficulty(GameMode.Difficulty.Hard);
        GameMode.ScaleChanceToDifficulty(GameMode.GetDifficulty());
        DifficultyText.text = GameMode.GetDifficulty().ToString();
    }

    public void goToChangeDifficultyMenuBtn()
    {
        Time.timeScale = 0;
    }
    public void StartGameBtn()
    {
        Time.timeScale = 1.0f;
    }

}
