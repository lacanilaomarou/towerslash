using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter : MonoBehaviour
{
    [SerializeField] public int HP = 1;
    [SerializeField] private float speed;

    private Rigidbody2D rb;
    public ConstantForce2D customGravity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void damageHP(int Damage)
    {
        HP -= Damage;
        if (HP <= 0)
        {
            OnDeath();
        }
    }

    public void addHP(int Heal) 
    { 
        HP += Heal;
        if (HP > 5) HP = 5;
    }
    public float getSpeed() { return speed; }
    public void setSpeed(float newSpeed) { speed = newSpeed; }
    public Rigidbody2D GetRB() { return rb; }
    public void setRB(Rigidbody2D r) { rb = r; }

    public int getHp() { return HP; }

    public virtual void CharacterMovement() { }

    public virtual void OnAttack(GameObject attackedCharacter, BaseCharacter attackedClass) { }

    public virtual void OnDeath() { }
}
