using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeAttack : MonoBehaviour
{
    private SwipeGestures Swipe;
    private PlayerCharacter Player;

    private GameObject EnemyObj;
    private EnemyBase EnemyClass;

    [SerializeField] private bool isEnemyNearby;
    [SerializeField] private bool isBossNearby;
    [SerializeField] private Types.ArrowType enemyType;
    [SerializeField] private Types.DirectionType enemyDir;

    [SerializeField] private Types.ArrowType enemyType_2;
    [SerializeField] private Types.DirectionType enemyDir_2;

    // Start is called before the first frame update
    void Start()
    {
        Swipe = GetComponentInParent<SwipeGestures>();
        Player = GetComponentInParent<PlayerCharacter>();

        ResetValues();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void AtkEnemy(GameObject Enemy, EnemyBase enemyBase, Types.DirectionType Dir)
    {
        if (Dir == Swipe.getDirection())
        {
            enemyBase.damageHP(1);
        }
        else if(Dir != Swipe.getDirection())
        {
            Player.damageHP(1);
        }
    }

    private void AtkBoss(GameObject Boss, Enemy_Boss BossCLass, Types.DirectionType bossDir_1, Types.DirectionType bossDir_2)
    {
        //if(BossCLass.getArrowObj() !=null)
        //{
        //    if (bossDir_1 == Swipe.getDirection())
        //    {
        //        BossCLass.damageHP(1);
        //    }
        //    else if (bossDir_1 != Swipe.getDirection())
        //    {
        //        Player.damageHP(1);
        //    }
        //}
        //else if(BossCLass.getArrowObj() == null &&
        //    BossCLass.getArrowObject_2() !=null)
        //{
        //    Debug.Log("Second Sign");
        //    if (bossDir_2 == Swipe.getDirection())
        //    {
        //        BossCLass.damageHP(1);
        //    }
        //    else if (bossDir_2 != Swipe.getDirection())
        //    {
        //        Player.damageHP(1);
        //    }
        //}

        if (bossDir_1 == Swipe.getDirection() || bossDir_2 == Swipe.getDirection())
        {
            if(BossCLass.getArrowObj() != null &&
                bossDir_1 == Swipe.getDirection())
            {
                BossCLass.damageBoss(1, true);
            }
            else if (BossCLass.getArrowObject_2() != null &&
                bossDir_2 == Swipe.getDirection())
            {
                BossCLass.damageBoss(1, false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            ResetValues();
            EnemyObj = collision.gameObject;
            EnemyClass = EnemyObj.GetComponent<EnemyBase>();

            enemyDir = EnemyClass.GetDirectionType();
            enemyType = EnemyClass.GetArrowType();

            if (enemyType == Types.ArrowType.Red)
            {
                int i = (int)enemyDir * -1;
                enemyDir = (Types.DirectionType)i;
            }
            Debug.Log("Arrow1: " + enemyType + " , " + enemyDir);
            isEnemyNearby = true;

            if (EnemyClass.getIsBoss())
            {
                isBossNearby = true;
                Enemy_Boss BossClass = (Enemy_Boss)EnemyClass;
                enemyDir_2 = BossClass.getDirection_2();
                enemyType_2 = BossClass.GetArrowType_2();

                if (enemyType_2 == Types.ArrowType.Red)
                {
                    int i = (int)enemyDir_2 * -1;
                    enemyDir_2 = (Types.DirectionType)i;
                }
                Debug.Log("Arrow2: " + enemyType_2 + " , " + enemyDir_2);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            if (isEnemyNearby)
            {
                if(Swipe.getDirection() != Types.DirectionType.None && 
                    Swipe.getDirection() != Types.DirectionType.Tap)
                {
                    if(isBossNearby)
                    {
                        AtkBoss(EnemyObj, (Enemy_Boss)EnemyClass, enemyDir, enemyDir_2);
                    }
                    else
                    {
                        AtkEnemy(EnemyObj, EnemyClass, enemyDir);
                    }
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ResetValues();
    }

    private void ResetValues()
    {
        EnemyObj = null;
        EnemyClass = null;

        isEnemyNearby = false;
        isBossNearby = false;

        enemyType = Types.ArrowType.Null;
        enemyDir = Types.DirectionType.None;

        enemyType_2 = Types.ArrowType.Null;
        enemyDir_2 = Types.DirectionType.None;
    }
}
