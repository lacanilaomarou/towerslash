using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    [SerializeField] GameObject TowerPrefab;
    [SerializeField] Transform NextTowerLocation;

    [SerializeField] GameObject Enemy_Prefab;
    [SerializeField] GameObject Boss_Prefab;
    [SerializeField] GameObject PickupHP_Prefab;
    [SerializeField] GameObject PickupSlowMo_Prefab;
    [SerializeField] Transform[] SpawnLocations = new Transform[5];

    private GameMode gameMode;
    private bool isDoneSpawning;
    void Start()
    {
        isDoneSpawning = false;
        //GameMode.getChanceHP(), GameMode.getChanceSlowMo(), GameMode.getChanceBoss()
        Debug.Log("HP C: "+ GameMode.getChanceHP() +  " , SM C:" + GameMode.getChanceSlowMo() + " ,BSS C: " + GameMode.getChanceBoss());

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private GameObject InitObject(GameObject Prefab, Transform location)
    {
        GameObject obj = Instantiate(Prefab, location.position, location.rotation);
        return obj;
    }

    public void PopulateTower(float chanceHP, float chanceSlowMo, float chanceBoss)
    {
        for(int i =0; i<SpawnLocations.Length;i++)
        {
            float rng = Random.Range(0, 1.0f);

            if(rng<= chanceHP)
            {
                InitObject(PickupHP_Prefab, SpawnLocations[i]);
            }
            else if(rng <= chanceSlowMo)
            {
                InitObject(PickupSlowMo_Prefab, SpawnLocations[i]);
            }
            else if(rng <= chanceBoss)
            {
                InitObject(Boss_Prefab, SpawnLocations[i]);
            }
            else
            {
                InitObject(Enemy_Prefab, SpawnLocations[i]);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(!isDoneSpawning)
            {
                SpawnNextTower();
                isDoneSpawning = true;
            }

        }
    }

    public void SpawnNextTower()
    {
        GameObject NextTower = Instantiate(TowerPrefab, NextTowerLocation.position, NextTowerLocation.rotation);
        NextTower.transform.SetParent(this.transform.parent);
        TowerScript NTScript = NextTower.GetComponent<TowerScript>();
        NTScript.PopulateTower(GameMode.getChanceHP(), GameMode.getChanceSlowMo(), GameMode.getChanceBoss());
        StartCoroutine(removeThisTower());
    }

    private IEnumerator removeThisTower()
    {
        yield return new WaitForSeconds(30.0f);
        Destroy(this);
    }
}
