using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Regular : EnemyBase
{
    void Start()
    {
        isBoss = false;
        int index = Random.Range(0, Numbers.Length);
        Direction = (Types.DirectionType)Numbers[index];

        arrowType = (Types.ArrowType)Random.Range(0, 2);

        ArrowObj = InitSign(Arrow_Location, Direction, arrowType);

        if(arrowType == Types.ArrowType.Rotating)
        {
            isRotatingSign = true;
            StartCoroutine(SpinningSign(ArrowObj,index));
        }
    }
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (ArrowObj != null)
            {
                isRotatingSign = false;
                GiveArrowColor(ArrowObj, arrowType);
            }
        }
    }

    public override void OnDeath()
    {
        GameMode.addScore(10);
        Debug.Log(GameMode.getScore());
        base.OnDeath();
    }

}
