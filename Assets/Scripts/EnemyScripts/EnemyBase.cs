using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : BaseCharacter
{
    //offset -2.1765
    //size 3.146905
    public GameObject Arrow_Prefab;
    public Transform Arrow_Location;

    [HideInInspector] public int[] Numbers = { 1, 2, -1, -2 };
    protected GameObject ArrowObj;
    protected Types.DirectionType Direction;
    protected Types.ArrowType arrowType;
    protected bool isRotatingSign = false;
    [SerializeField]protected bool isBoss = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnDeath()
    {
        base.OnDeath();

        Destroy(this.gameObject);
    }

    protected void GiveArrowColor(GameObject obj, Types.ArrowType arrowColor)
    {
        switch(arrowColor)
        {
            case Types.ArrowType.Green:
            case Types.ArrowType.Rotating:
                obj.GetComponent<SpriteRenderer>().color = new Color(0.0f, 250.0f, 0.0f);
                break;
            case Types.ArrowType.Red:
                obj.GetComponent<SpriteRenderer>().color = new Color(250.0f, 0.0f, 0.0f);
                break;
        }
    }

    protected void RotateSign(GameObject obj, Types.DirectionType dir)
    {
        float rot = 0.0f;
        switch(dir)
        {
            case Types.DirectionType.Down:
                rot = 270.0f;
                break;
            case Types.DirectionType.Left:
                rot = 180.0f;
                break;
            case Types.DirectionType.Right:
                rot = 0.0f;
                break;
            case Types.DirectionType.Up:
                rot = 90.0f;
                break;
        }
        obj.transform.rotation = Quaternion.Euler(0, 0, rot);
    }

    protected GameObject InitSign(Transform location, Types.DirectionType dir, Types.ArrowType type)
    {
        GameObject obj = Instantiate(Arrow_Prefab, location.position, location.rotation);
        obj.transform.parent = this.transform;

        RotateSign(obj, dir);
        return obj;
    }

    protected IEnumerator SpinningSign(GameObject obj,int index)
    {
        while (isRotatingSign)
        {
            index++;
            if (index >= Numbers.Length) index = 0;
            Direction = (Types.DirectionType)Numbers[index];
            RotateSign(obj, Direction);

            yield return new WaitForSeconds(0.3f);
        }
    }
    public GameObject getArrowObj() { return ArrowObj; }
    public Types.DirectionType GetDirectionType() { return Direction; }
    public Types.ArrowType GetArrowType() { return arrowType; }
    public bool getIsRotating() { return isRotatingSign; }
    public void setIsRotating(bool value) { isRotatingSign = value; }
    public bool getIsBoss() { return isBoss; }

    
}
