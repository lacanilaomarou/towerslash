using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Boss : EnemyBase
{
    [SerializeField] Transform Arrow_Location_2;
    private GameObject ArrowObj_2;
    private Types.DirectionType Direction_2;
    private Types.ArrowType ArrowType_2;
    
    // Start is called before the first frame update
    void Start()
    {
        isBoss = true;
        //first arrow
        int index = Random.Range(0, Numbers.Length);
        Direction = (Types.DirectionType)Numbers[index];

        arrowType = (Types.ArrowType)Random.Range(0, (int)Types.ArrowType.MAX);

        ArrowObj = InitSign(Arrow_Location, Direction, arrowType);

        if (arrowType == Types.ArrowType.Rotating)
        {
            isRotatingSign = true;
            StartCoroutine(SpinningSign(ArrowObj, index));
        }


        //second arrow
        int i = Random.Range(0, Numbers.Length);
        Direction_2 = (Types.DirectionType)Numbers[i];

        ArrowType_2 = (Types.ArrowType)Random.Range(0, (int)Types.ArrowType.MAX-1);

        ArrowObj_2 = InitSign(Arrow_Location_2, Direction_2, ArrowType_2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void damageHP(int Damage)
    {
        base.damageHP(Damage);
    }

    public void damageBoss(int damage, bool isFirst)
    {
        damageHP(damage);
        if (HP > 0)
        {
            if(isFirst)
            {
                Destroy(ArrowObj);
            }
            else if(!isFirst)
            {
                Destroy(ArrowObj_2);
            }
        }
    }

    public override void OnDeath()
    {
        GameMode.addScore(15);
        base.OnDeath();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (ArrowObj != null)
            {
                isRotatingSign = false;
                GiveArrowColor(ArrowObj, arrowType);
                GiveArrowColor(ArrowObj_2, ArrowType_2);
            }
        }
    }

    public Types.DirectionType getDirection_2() { return Direction_2; }
    public Types.ArrowType GetArrowType_2() { return ArrowType_2; }
    public GameObject  getArrowObject_2() { return ArrowObj_2; }
}
