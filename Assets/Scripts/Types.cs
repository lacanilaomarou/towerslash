using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types : MonoBehaviour
{
    // Start is called before the first frame update
    public enum DirectionType
    {
        Down = -2,
        Left = -1,
        None = 0,
        Right = 1,
        Up = 2,
        Tap = 3,
    }

    public enum ArrowType
    {
        Null = -1,
        Green = 0,
        Red = 1,
        Rotating = 2,
        MAX = 3,
    }
}
