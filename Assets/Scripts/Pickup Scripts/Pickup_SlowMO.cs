using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_SlowMO : MonoBehaviour
{
    [SerializeField] private float Duration = 5.0f;
    [SerializeField] private float Scale =  0.8f;
    public bool isActive = false;
    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if(collision.gameObject.GetComponent<PlayerCharacter>() != null)
            {
               if(!isActive)
                {
                    PlayerCharacter player = collision.gameObject.GetComponent<PlayerCharacter>();

                    StartCoroutine(Slow(player));
                }
                
            }
        }
    }

    IEnumerator SlowMotion()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        Time.timeScale = Scale;
        isActive = true;
        yield return new WaitForSecondsRealtime(Duration);
        isActive = false;
        Time.timeScale = 1;
        Destroy(this.gameObject);
    }

    IEnumerator Slow(PlayerCharacter player)
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        float defaultSpeed = player.getSpeed();
        player.setSpeed(defaultSpeed * Scale);
        isActive = true;
        yield return new WaitForSecondsRealtime(Duration);
        isActive = false;
        player.setSpeed(defaultSpeed);
        Destroy(this.gameObject);
    }
}
