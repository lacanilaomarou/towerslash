using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_HP : MonoBehaviour
{
    [SerializeField] private int HealValue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Playerenter");
            if (collision.gameObject.GetComponent<PlayerCharacter>() != null)
            {


                PlayerCharacter playerCharacter = collision.gameObject.GetComponent<PlayerCharacter>();

                playerCharacter.addHP(HealValue);

                Destroy(this.gameObject);
            }
        }
    }
}
