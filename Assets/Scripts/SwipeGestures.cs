using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeGestures : MonoBehaviour
{
    [SerializeField] public Types.DirectionType swipeDirection; 

    private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
    private bool isDragging = false;
    private float startTimer;
    private float endTimer;
    private Vector2 startTouch, swipeDelta;

    void Update()
    {
        tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;
        swipeDirection = Types.DirectionType.None;

        mouseInput();

        mobileInput();

        calculateDistance();

        if(endTimer<0.1f && endTimer  >0)
        {
            swipeDirection = Types.DirectionType.Tap;
            endTimer = 0;
            Reset();
        }
        else
        {
            checkDirection();
        }

    }

    private void Reset()
    {
        isDragging = false;
        startTouch = swipeDelta = Vector2.zero;
    }

    private void mouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            isDragging = true;
            startTouch = Input.mousePosition;
            startTimer = Time.realtimeSinceStartup;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
            Reset();
            endTimer = Time.realtimeSinceStartup - startTimer;
        }
    }

    private void mobileInput()
    {
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                tap = true;
                isDragging = true;
                startTouch = Input.touches[0].position;
                startTimer = Time.time;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDragging = false;
                endTimer = Time.time - startTimer;
                Reset();
            }
        }
    }

    private void calculateDistance()
    {
        swipeDelta = Vector2.zero;
        if (isDragging)
        {
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }
    }

    private void checkDirection()
    {
        if (swipeDelta.magnitude > 100.0f)
        {
            //check direction
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            //left or right
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                //left
                if (x < 0)
                {
                    swipeLeft = true;
                    swipeDirection = Types.DirectionType.Left;
                }
                //right
                else
                {
                    swipeRight = true;
                    swipeDirection = Types.DirectionType.Right;
                }
            }
            //up or down
            else
            {
                //down
                if (y < 0)
                {
                    swipeDown = true;
                    swipeDirection = Types.DirectionType.Down;
                }
                //up
                else
                {
                    swipeUp = true;
                    swipeDirection = Types.DirectionType.Up;
                }
            }
            Reset();
        }
    }

    public bool getTap() { return tap; }
    public bool getIsDragging() { return isDragging; }  
    public bool getSwipeLeft() { return swipeLeft; }
    public bool getSwipeRight() { return swipeRight; }
    public bool getSwipeUp() { return swipeUp; }
    public bool getSwipeDown() { return swipeDown; }

    public Types.DirectionType getDirection()
    {
        return swipeDirection;
    }
}