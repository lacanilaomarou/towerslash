using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] Transform playerTransform;
    Vector3 tempVector = new Vector3();
    // Start is called before the first frame update
    void Start()
    {
        tempVector.z = -1.0f;
        tempVector.x = this.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        
        tempVector.y = playerTransform.position.y + 4.0f;
        this.transform.position = tempVector;
    }
}
